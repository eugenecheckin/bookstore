import React, { useState, useEffect } from 'react';
import {
  BrowserRouter, Link, Route, Routes,
} from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useCookies } from 'react-cookie';

import { BottomNavigationAction, Typography, BottomNavigation } from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import PersonIcon from '@mui/icons-material/Person';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import HowToRegIcon from '@mui/icons-material/HowToReg';
import AddToDriveIcon from '@mui/icons-material/AddToDrive';
import NotificationsIcon from '@mui/icons-material/Notifications';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

import SignUpPage from './Pages/SignUp';
import Main from './Pages/Main';
import SignInPage from './Pages/SignIn';
import ProfilePage from './Pages/Profile/index';
import CartPage from './Pages/Cart';
import AddBookPage from './Pages/Admin';
import { RootState } from './Store/store';
import BookPage from './Pages/Book';
import {
  loadAutors, loadBooks, loadGenres, loadPrice,
} from './Thunk';
import socket from './socket';

import AppHeader from './app.style';
import Wrapper from './Pages/common.style';

// eslint-disable-next-line react/function-component-definition
const App: React.FC = () => {
  const dispatch = useDispatch();
  // eslint-disable-next-line no-unused-vars
  const [cookies, setCookie, removeCookie] = useCookies(['token']);
  const ownerName: string = useSelector((state: RootState) => state.OwnerData.name);
  const isAdmin = useSelector((state: RootState) => state.OwnerData.isAdmin);
  const [repost, setRepost] = useState({
    userID: '',
    email: '',
    from: '',
    text: '',
  });

  useEffect(() => {
    socket.on('private message', ({
      userID, email, from, text,
    }) => {
      setRepost((prev) => ({ ...prev, userID }));
      setRepost((prev) => ({ ...prev, email }));
      setRepost((prev) => ({ ...prev, from }));
      setRepost((prev) => ({ ...prev, text }));
      console.log(`${userID}: ${email} ${from} ${text}`);
    });
  }, []);
  const logOut = () => {
    removeCookie('token', { path: '/' });
    dispatch({
      type: 'RESET',
    });
    dispatch(loadBooks(0));
    dispatch(loadAutors());
    dispatch(loadGenres());
    dispatch(loadPrice());
  };

  const [open, setOpen] = React.useState(false);
  const handleClick = () => {
    setOpen(true);
  };
  const handleClose: any = (event: React.SyntheticEvent, reason: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
    setRepost((prev) => ({ ...prev, userID: '' }));
    setRepost((prev) => ({ ...prev, email: '' }));
    setRepost((prev) => ({ ...prev, from: '' }));
    setRepost((prev) => ({ ...prev, text: '' }));
  };

  const action = (
    <IconButton
      size="small"
      aria-label="close"
      color="inherit"
      onClick={handleClose}
    >
      <CloseIcon fontSize="small" />
    </IconButton>
  );

  return (
    <Wrapper>
      <BrowserRouter>
        <div className="app">
          <AppHeader>
            {ownerName && (
            // eslint-disable-next-line react/jsx-no-useless-fragment
            <Typography variant="h6" className="page-title">{`Добро пожаловать, ${ownerName}`}</Typography>
            )}
            <ul className="app-header">
              <li className="link-header">
                <Link to="/">
                  <BottomNavigation showLabels>
                    <BottomNavigationAction label="Главная" icon={<HomeIcon />} />
                  </BottomNavigation>
                </Link>
              </li>
              {!ownerName && (
                <li className="link-header">
                  <Link to="/signup">
                    <BottomNavigation showLabels>
                      <BottomNavigationAction label="Регистрация" icon={<HowToRegIcon />} />
                    </BottomNavigation>
                  </Link>
                </li>
              )}
              {!ownerName && (
                <li className="link-header">
                  <Link to="/signin">
                    <BottomNavigation showLabels>
                      <BottomNavigationAction label="Войти" icon={<LoginIcon />} />
                    </BottomNavigation>
                  </Link>
                </li>
              )}
              {ownerName && (
                <li className="link-header">
                  <Link to="/profile">
                    <BottomNavigation showLabels>
                      <BottomNavigationAction label="Профиль" icon={<PersonIcon />} />
                    </BottomNavigation>
                  </Link>
                </li>
              )}
              {ownerName && (
                <li className="link-header">
                  <Link to="/cart">
                    <BottomNavigation showLabels>
                      <BottomNavigationAction label="Корзина" icon={<ShoppingCartCheckoutIcon />} />
                    </BottomNavigation>
                  </Link>
                </li>
              )}
              {isAdmin && (
                <li className="link-header">
                  <Link to="/addbook">
                    <BottomNavigation showLabels>
                      <BottomNavigationAction label="Добавить книгу" icon={<AddToDriveIcon />} />
                    </BottomNavigation>
                  </Link>
                </li>
              )}
              {ownerName && <BottomNavigation showLabels><BottomNavigationAction label="Выйти" icon={<LogoutIcon onClick={logOut} />} /></BottomNavigation>}

              {!repost.text && <BottomNavigation showLabels><BottomNavigationAction label="" icon={<NotificationsIcon />} /></BottomNavigation>}
              {repost.text && <BottomNavigation showLabels><BottomNavigationAction label="" icon={<NotificationsActiveIcon onClick={handleClick} />} /></BottomNavigation>}
              <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={repost.text}
                action={action}
              />
            </ul>
          </AppHeader>
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="/signup" element={<SignUpPage />} />
            <Route path="/signin" element={<SignInPage />} />
            <Route path="/profile" element={<ProfilePage />} />
            <Route path="/cart" element={<CartPage />} />
            <Route path="/addbook" element={<AddBookPage />} />
            <Route path="/book/:bookid" element={<BookPage />} />
          </Routes>
        </div>
      </BrowserRouter>
    </Wrapper>
  );
};

export default App;
