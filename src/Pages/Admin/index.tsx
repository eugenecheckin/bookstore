import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Button, Input, Typography } from '@mui/material';

import { RootState } from '../../Store/store';
import { addBook } from '../../Thunk';

import AddBook from './style';
import logo from './defaultCover.png';

// eslint-disable-next-line react/function-component-definition
const AddBookPage: React.FC = () => {
  const dispatch = useDispatch();

  const [bookUpData, setBookData] = useState({
    title: '',
    autor: '',
    year: '',
    genre: '',
    price: '',
  });

  const [bookCover, setbookCover] = useState({});

  const ownerData = useSelector((state: RootState) => state.OwnerData);

  const buttonClickEvHandler = () => {
    dispatch(addBook(bookUpData, bookCover, `Bearer ${ownerData.token}`));
  };

  const onChangeCover = (e: any) => {
    if (!e.target.files) return;
    const file = e.target.files[0];
    setbookCover(file);

    // eslint-disable-next-line no-undef
    const preview: HTMLImageElement = document.getElementById(
      'prevLogo',
    // eslint-disable-next-line no-undef
    ) as HTMLImageElement;
    // eslint-disable-next-line no-undef
    const reader = new FileReader();
    reader.onloadend = () => {
      preview.src = reader.result as string;
    };
    if (file) {
      reader.readAsDataURL(file);
    } else {
      preview.src = logo;
    }
  };

  return (
    <AddBook>
      <Typography variant="h5" className="page-title">
        Добавить книгу
      </Typography>
      <div className="addbook-container">
        <Input
          type="text"
          className="add-title"
          placeholder="Название"
          onChange={(e) => setBookData((prev) => ({ ...prev, title: e.target.value }))}
        />
        <Input
          type="text"
          className="add-autor"
          placeholder="Автор"
          onChange={(e) => setBookData((prev) => ({ ...prev, autor: e.target.value }))}
        />
        <Input
          type="datetime-local"
          className="add-year"
          placeholder="Год"
          onChange={(e) => setBookData((prev) => ({ ...prev, year: e.target.value }))}
        />
        <Input
          type="text"
          className="add-genre"
          placeholder="Жанр"
          onChange={(e) => setBookData((prev) => ({ ...prev, genre: e.target.value }))}
        />
        <img id="prevLogo" src={logo} alt="" />
        <input type="file" name="filedata" onChange={onChangeCover} />
        <Input
          type="text"
          className="add-price"
          placeholder="Цена"
          onChange={(e) => setBookData((prev) => ({ ...prev, price: e.target.value }))}
        />

        <Button onClick={buttonClickEvHandler}>Добавить</Button>
      </div>
    </AddBook>
  );
};

export default AddBookPage;
