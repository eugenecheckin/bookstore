import styled from 'styled-components';

const AddBook = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .addbook-container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .page-title {
    padding: 5px;
    margin: 0 auto;
  }
`;
export default AddBook;
