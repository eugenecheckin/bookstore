import * as React from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useState } from 'react';

import {
  Avatar, Grid, Paper,
} from '@material-ui/core';
import {
  Box, Button, TextField,
} from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import { RootState } from '../../Store/store';
import { addComment } from '../../Thunk';
import { REMOTE_URL } from '../../config';
import socket from '../../socket';

import imgLink from './defaultAvatar.svg';
// eslint-disable-next-line react/function-component-definition
const Comment: React.FC = ({ allState }: any) => {
  const { bookid } = useParams();
  const findedbook = allState.Books.rows.find((i:any) => i.id === Number(bookid));
  const allComments = allState.Comments;
  const ownerData = useSelector((state: RootState) => state.OwnerData);
  const [comment, setComment] = useState({
    comment: '',
    bookid: '',
    to: '',
    email: '',
  });
  const dispatch = useDispatch();
  const buttonClickEvHandler = () => {
    dispatch(addComment(`Bearer ${ownerData.token}`, comment));
    socket.emit('repost', {
      to: comment.email,
      from: ownerData.email,
      text: comment.comment,
    });
    setComment((prev) => ({ ...prev, comment: '' }));
    setComment((prev) => ({ ...prev, bookid: '' }));
    setComment((prev) => ({ ...prev, to: '' }));
  };

  interface onAddCommInputEvent {
    target:{
      value:string
    }
  }

  return (
    <div style={{ padding: 14 }} className="App">
      <h1>Комментарии</h1>
      { (ownerData.token !== undefined) ? (
        <>
          <Box
            component="form"
            sx={{
              '& .MuiTextField-root': { m: 1, width: '50ch' },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-multiline-flexible"
              label="Комментарий"
              multiline
              maxRows={4}
              value={comment.comment}
              onChange={(e: onAddCommInputEvent) => {
                setComment((prev) => ({ ...prev, comment: e.target.value }));
                setComment((prev) => ({ ...prev, bookid: findedbook.id }));
                setComment((prev) => ({ ...prev, to: '' }));
              }}
            />
          </Box>
          <Button onClick={buttonClickEvHandler}>Добавить комментарий</Button>
        </>
      )
        : <>Для добавления комментария пожалуйста авторизуйтесь.</>}
      {(allComments.length > 0) ? (
        allComments.map((item: any) => (
          // eslint-disable-next-line react/jsx-no-useless-fragment
          <div className="commItem" key={`${item.id}-commItem`}>
            {(item.to === null) && (
              <Paper style={{ padding: '40px 20px', marginTop: 50 }} key={`${item.id}-comm`}>
                <Grid container wrap="nowrap" spacing={2}>
                  <Grid item>
                    <Avatar alt="Remy Sharp" src={(item.Users.avatar === null) ? imgLink : `${`${REMOTE_URL}fileserver/${item.Users.avatar}`}`} />
                  </Grid>
                  <Grid>
                    <h4 style={{ margin: 0, textAlign: 'left' }}>{item.Users.fullName}</h4>
                    <p style={{ textAlign: 'left' }}>
                      {item.comment}
                    </p>
                    <p style={{ textAlign: 'left', color: 'gray' }} />
                  </Grid>
                </Grid>
                {(ownerData.token !== undefined) ? (
                  <Accordion>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography>Ответить</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <>
                        <Box
                          component="form"
                          sx={{
                            '& .MuiTextField-root': { m: 1, width: '50ch' },
                          }}
                          noValidate
                          autoComplete="off"
                        >
                          <TextField
                            id="outlined-multiline-flexible"
                            label="Комментарий"
                            multiline
                            maxRows={4}
                            value={comment.comment}
                            onChange={(e: onAddCommInputEvent) => {
                              setComment((prev) => ({ ...prev, comment: e.target.value }));
                              setComment((prev) => ({ ...prev, bookid: findedbook.id }));
                              setComment((prev) => ({ ...prev, to: item.id }));
                              setComment((prev) => ({ ...prev, email: item.Users.email }));
                            }}
                          />
                        </Box>
                        <Button onClick={buttonClickEvHandler}>Добавить комментарий</Button>
                      </>
                    </AccordionDetails>
                  </Accordion>
                )
                  : <>Для добавления комментария пожалуйста авторизуйтесь.</>}

                {(item.from > 0)
                  && (
                    <Paper style={{ padding: '40px 20px', marginTop: 50 }} key={`${allComments.find((el: any) => el.id === item.from).id}-child`}>
                      <Grid container wrap="nowrap" spacing={2}>
                        <Grid item>
                          <Avatar alt="Remy Sharp" src={(allComments.find((el: any) => el.id === item.from).Users.avatar === null) ? imgLink : `${`${REMOTE_URL}fileserver/${allComments.find((el: any) => el.id === item.from).Users.avatar}`}`} />
                        </Grid>
                        <Grid>
                          <h4 style={{ margin: 0, textAlign: 'left' }}>{allComments.find((el: any) => el.id === item.from).Users.fullName}</h4>
                          <p style={{ textAlign: 'left' }}>
                            {allComments.find((el: any) => el.id === item.from).comment}
                          </p>
                          <p style={{ textAlign: 'left', color: 'gray' }} />
                        </Grid>
                      </Grid>
                    </Paper>
                  ) }
              </Paper>
            )}
          </div>
        ))
      )
        : <>Комментарии отсутствуют</>}
    </div>
  );
};

function mapStateToProps(state: RootState) {
  return { allState: state };
}

export default connect(mapStateToProps, null)(Comment);
