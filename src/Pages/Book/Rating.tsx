import React from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector, connect } from 'react-redux';

import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

import { RootState } from '../../Store/store';
import { addRating } from '../../Thunk';

// eslint-disable-next-line react/function-component-definition
const BasicRating: React.FC = ({ allState }: any) => {
  const { bookid } = useParams();
  const ownerData = useSelector((state: RootState) => state.OwnerData);
  const rating: number = allState.Rating.resultRating;
  const dispatch = useDispatch();
  return (
    <Box
      sx={{
        '& > legend': { mt: 2 },
      }}
    >
      { (rating !== undefined) ? (
        // eslint-disable-next-line react/jsx-no-useless-fragment
        <>
          { (ownerData.token !== undefined) ? (
            <>
              <Typography component="legend">Изменить оценку</Typography>
              <Rating
                name="simple-controlled"
                value={rating}
                onChange={(event: React.SyntheticEvent, newValue: any) => {
                  dispatch(addRating(`Bearer ${ownerData.token}`, { bookid, rating: newValue }));
                }}
              />
            </>
          )
            : (
              <>
                <Typography component="legend">Рейтинг</Typography>
                <Rating name="read-only" value={rating} readOnly />
              </>
            )}

        </>
      )
        : <>идет загрузка</>}
    </Box>
  );
};

function mapStateToProps(state: RootState) {
  return { allState: state };
}

export default connect(mapStateToProps, null)(BasicRating);
