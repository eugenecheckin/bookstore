import * as React from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';

import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

import { RootState } from '../../Store/store';

// eslint-disable-next-line react/function-component-definition
const LabTabs: React.FC = ({ allState }: any) => {
  const [value, setValue] = React.useState('1');
  const { bookid } = useParams();
  const findedbook = allState.rows.find((i:any) => i.id === Number(bookid));

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%', typography: 'body1' }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label="О книге" value="1" />
            <Tab label="Характеристики" value="2" />
          </TabList>
        </Box>
        <TabPanel value="1">
          { (findedbook) !== undefined
            ? (
              <>
                <div>{`Название: "${findedbook.title}"`}</div>
                <div>{`Автор: "${findedbook.autor}"`}</div>
              </>
            )
            : (<>Идет загрузка</>)}
        </TabPanel>
        <TabPanel value="2">
          { (findedbook) !== undefined
            ? (
              <>
                <div>{`Цена: ${findedbook.price} Р`}</div>
                <div>{`Жанр: "${findedbook.genre}"`}</div>
              </>
            )
            : (<>Идет загрузка</>)}
        </TabPanel>
      </TabContext>
    </Box>
  );
};

function mapStateToProps(state: RootState) {
  return { allState: state.Books };
}

export default connect(mapStateToProps, null)(LabTabs);
