import React, { useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useCookies } from 'react-cookie';

import { Paper, Typography } from '@mui/material';
import { REMOTE_URL } from '../../config';
import { RootState } from '../../Store/store';
import LabTabs from './Tab';
import BasicRating from './Rating';
import Comment from './Comments';
import {
  loadAutors, loadBooks, loadCart, loadComment, loadGenres, loadPrice, loadRating, login,
} from '../../Thunk';
import socket from '../../socket';

import Books from './style';

// eslint-disable-next-line react/function-component-definition
const BookPage: React.FC = ({ allState }: any) => {
  const { bookid } = useParams();
  const dispatch = useDispatch();
  const [cookies, setCookie] = useCookies(['token']);
  const onConnect = (email:string) => {
    // this.usernameAlreadySelected = true;
    socket.auth = { email };
    socket.connect();
  };
  useEffect(() => {
    dispatch(loadComment({ bookid }));
    dispatch(loadRating({ bookid }));
    dispatch(loadBooks(0));
    dispatch(loadAutors());
    dispatch(loadGenres());
    dispatch(loadPrice());
  }, []);
  useEffect(() => {
    if (allState.OwnerData.token !== undefined) {
      onConnect(allState.OwnerData.email);
      setCookie('token', allState.OwnerData.token, { path: '/' });
      dispatch(loadCart(`Bearer ${allState.OwnerData.token}`));
    } else if (cookies.token !== undefined) {
      dispatch(loadCart(`Bearer ${cookies.token}`));
      dispatch(login(`Bearer ${cookies.token}`));
    }
    dispatch(loadComment({ bookid }));
    dispatch(loadRating({ bookid }));
  }, [allState.OwnerData.token]);

  return (
    <div className="сart">
      <header className="сart-header" />
      <div>
        <p>Книга</p>
        <Books>
          <div className="books">
            <Paper className="book">
              { (allState.Books.count) !== undefined
                ? (
                  <img
                    src={`${`${REMOTE_URL}fileserver/${allState.Books.rows.find((i:any) => i.id === Number(bookid)).cover}`}`}
                    alt="без обложки"
                  />
                )
                : (<>Идет загрузка</>)}
            </Paper>
          </div>
        </Books>
      </div>
      {(allState.Books.count !== undefined) ? (<LabTabs />) : ''}
      {(allState.Books.count !== undefined) ? (<BasicRating />) : ''}
      {(allState.Books.count !== undefined) ? (<Comment />) : ''}
      <div className="cart-footer">
        <Typography className="page-title">
          Все цены на сайте указаны в российских рублях с учетом НДС и без учета стоимости доставки.
          Цены на сайте могут отличаться от цен в розничных магазинах.
          Опция бесплатной доставки от 3000 рублей не распространяется на срочный способ доставки.
        </Typography>
      </div>
    </div>
  );
};

function mapStateToProps(state: RootState) {
  return { allState: state };
}

export default connect(mapStateToProps, null)(BookPage);
