import React, { useEffect } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useCookies } from 'react-cookie';

import { Button } from '@mui/material';
import { Paper } from '@material-ui/core';

import { loadCart, removeCart } from '../../Thunk';
import { RootState } from '../../Store/store';
import { REMOTE_URL } from '../../config';

import Books from './style';

// eslint-disable-next-line react/function-component-definition
const CartPage: React.FC = ({ userCart }:any) => {
  const dispatch = useDispatch();
  const ownerData = useSelector((state: RootState) => state.OwnerData);
  const [cookies, setCookie] = useCookies(['token']);
  useEffect(() => {
    dispatch(loadCart(`Bearer ${ownerData.token}`));
  }, []);

  const removeToCart = (e: any) => {
    const btnId = e.target.id;
    const bookId = btnId.split('-')[0];
    dispatch(removeCart(`Bearer ${cookies.token}`, bookId));
  };
  return (
    <div className="сart">
      <header className="сart-header" />
      <div>
        <p>Моя корзина</p>
        { (userCart.length) > 0
          ? (
            <Books>
              <div className="books">
                {userCart.map((item: any) => (
                  <Paper className="book" key={`${item.id}-book`}>
                    <img
                      src={`${`${REMOTE_URL}fileserver/${item.cover}`}`}
                      alt="без обложки"
                    />
                    <div>{item.title}</div>
                    <div>{item.autor}</div>
                    <div>{`${item.price} Р`}</div>
                    <Button id={`${item.id}-btncart`} onClick={removeToCart}>Удалить из корзины</Button>
                  </Paper>
                ))}
              </div>
            </Books>
          )
          : (<>Корзина пуста</>)}
      </div>
    </div>
  );
};
function mapStateToProps(state: RootState) {
  return { userCart: state.Cart };
}

export default connect(mapStateToProps, null)(CartPage);
