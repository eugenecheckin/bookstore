import styled from 'styled-components';

const Books = styled.div`
  .books {
    display: flex;
    flex-wrap: wrap;
  }
  .book {
    padding: 5px;
    margin: 5px;
    border: 1px solid gray;
  }
`;
export default Books;
