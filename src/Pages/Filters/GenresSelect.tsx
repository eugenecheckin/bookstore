import * as React from 'react';
import { connect, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import { useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import { RootState } from '../../Store/store';
import { loadBooks } from '../../Thunk';
import urlParser from './types';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 25;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

// eslint-disable-next-line react/function-component-definition
const GenresSelect: React.FC = ({ allState }: any) => {
  function useQuery() {
    const { search } = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
  }
  const query = useQuery();
  const navigate = useNavigate();
  const theme = useTheme();
  const [personName, setPersonName] = React.useState<string[]>([]);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (query.get('genreId') !== null) {
      const intArrGenres = query.get('genreId')!.split('-');
      const inLineFilter: string[] = [];
      allState.BooksGenres.forEach((item: any) => {
        if (intArrGenres.indexOf(String(item.id)) !== -1) {
          inLineFilter.push(item.name);
        }
      });
      setPersonName(inLineFilter);
    }
  }, []);

  interface onSelectEvent {
    target:{
      value:string
    }
  }
  const handleChange = (event: onSelectEvent) => {
    const {
      target: { value },
    } = event;

    const arrGenre = typeof value === 'string' ? value.split(',') : value;
    const selectedBooksGenres = arrGenre.map((item:any) => allState.BooksGenres.find((elem:any) => {
      if (elem.name === item) {
        return item;
      }
    }));
    const indGenre: number[] = selectedBooksGenres.map((i:any) => i.id);

    const term = { genreId: indGenre.join('-') };
    const tstFilter = urlParser(term, query);
    navigate(`/${tstFilter}`);
    let toRequestBody: {[index: string]:string} = {};
    if (term.genreId !== '') {
      toRequestBody = { genreId: indGenre.join('-') };
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const [key, val] of query.entries()) {
      if (key !== 'genreId') {
        toRequestBody[key] = val;
      }
    }
    dispatch(loadBooks(0, toRequestBody));
    setPersonName(
      typeof value === 'string' ? value.split(',') : value,
    );
  };
  const names: [] = allState.BooksGenres.map((i: any) => i.name);

  function getStyles(name: any, genre: any, fontTheme: any) {
    return {
      fontWeight:
      genre.indexOf(name) === -1
        ? fontTheme.typography.fontWeightRegular
        : fontTheme.typography.fontWeightMedium,
    };
  }

  return (
    <div>
      <FormControl sx={{ m: 1, width: 230 }}>
        <InputLabel id="demo-multiple-name-label">Жанр</InputLabel>
        <Select
          labelId="demo-multiple-name-label"
          id="demo-multiple-name"
          multiple
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          value={personName}
          onChange={handleChange}
          input={<OutlinedInput label="Name" />}
          MenuProps={MenuProps}
        >
          {names.map((name: any) => (
            <MenuItem
              key={name}
              value={name}
              style={getStyles(name, personName, theme)}
            >
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

function mapStateToProps(state: RootState) {
  return { allState: state };
}

export default connect(mapStateToProps, null)(GenresSelect);
