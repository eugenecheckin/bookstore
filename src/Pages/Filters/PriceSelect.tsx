import * as React from 'react';
import { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';

import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';

import urlParser from './types';
import { RootState } from '../../Store/store';
import { loadBooks } from '../../Thunk';

// eslint-disable-next-line react/function-component-definition
const PriceSelect: React.FC = ({ allState }: any) => {
  const [value, setValue] = React.useState([0, 10000]);
  let startRange = allState.Price.min;
  let endRange = allState.Price.max;
  const navigate = useNavigate();
  const dispatch = useDispatch();

  function useQuery() {
    const { search } = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
  }
  const query = useQuery();

  const commitEvHanl = () => {
    const priceValue = `${value[0]}-${value[1]}`;
    const term = { price: priceValue };
    const tstFilter = urlParser(term, query);
    navigate(`/${tstFilter}`);
    const toRequestBody: {[index: string]:string} = { price: priceValue };
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, val] of query.entries()) {
      if (key !== 'price') {
        toRequestBody[key] = val;
      }
    }
    dispatch(loadBooks(0, toRequestBody));
  };
  useEffect(() => {
    if (query.get('price')) {
      startRange = Number(query.get('price')?.split('-')[0]);
      endRange = Number(query.get('price')?.split('-')[1]);
      setValue([startRange, endRange]);
    }
  }, []);

  const minDistance = 10;

  const handleChange = (event: Event, newValue: any, activeThumb: any) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    if (activeThumb === 0) {
      setValue([Math.min(newValue[0], value[1] - minDistance), value[1]]);
    } else {
      setValue([value[0], Math.max(newValue[1], value[0] + minDistance)]);
    }
  };

  return (
    <Box width={230}>
      <Slider
        getAriaLabel={() => 'Minimum distance'}
        min={startRange}
        max={endRange}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        onChangeCommitted={commitEvHanl}
        disableSwap
      />
    </Box>
  );
};
function mapStateToProps(state: RootState) {
  return { allState: state };
}

export default connect(mapStateToProps, null)(PriceSelect);
