const urlParse = (params:any, query: any) => {
  let newUrl = '';
  if (params[Object.keys(params)[0]] !== '') {
    newUrl = `?${Object.keys(params)[0]}=${params[Object.keys(params)[0]]}`;
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of query.entries()) {
    if (key !== Object.keys(params)[0]) {
      newUrl = newUrl.concat(`&${key}=${value}`);
    }
  }
  return newUrl;
};
export default urlParse;
