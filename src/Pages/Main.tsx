import React, { useEffect, useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useCookies } from 'react-cookie';

import { Paper } from '@material-ui/core';
import { Button, Pagination, Typography } from '@mui/material';

import {
  loadBooks, loadAutors, loadGenres, loadPrice, addCart, loadCart, removeCart, login,
} from '../Thunk';
import { REMOTE_URL } from '../config';
import AutorsSelect from './Filters/AutorsSelect';
import GenresSelect from './Filters/GenresSelect';
import PriceSelect from './Filters/PriceSelect';
import { RootState } from '../Store/store';

import Books from './main.style';

// eslint-disable-next-line react/function-component-definition
const Main: React.FC = ({ allState }: any) => {
  const dispatch = useDispatch();
  const [cookies, setCookie] = useCookies(['token']);
  const [currentPage, setPage] = useState(1);
  const navigate = useNavigate();

  function useQuery() {
    const { search } = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
  }
  const query = useQuery();

  const addToCart = (e: any) => {
    if (allState.OwnerData.token === undefined) {
      // eslint-disable-next-line no-undef
      alert('Добавление в корзину доступно только для авторизованных пользователей');
      return;
    }
    const btnId = e.target.id;
    const bookId = btnId.split('-')[0];
    dispatch(addCart(`Bearer ${allState.OwnerData.token}`, bookId));
  };

  const removeToCart = (e: any) => {
    if (allState.OwnerData.token === undefined) {
      // eslint-disable-next-line no-undef
      alert('Добавление в корзину доступно только для авторизованных пользователей');
      return;
    }
    const btnId = e.target.id;
    const bookId = btnId.split('-')[0];
    dispatch(removeCart(`Bearer ${allState.OwnerData.token}`, bookId));
  };

  useEffect(() => {
    if (allState.OwnerData.token !== undefined) {
      setCookie('token', allState.OwnerData.token, { path: '/' });
      dispatch(loadCart(`Bearer ${allState.OwnerData.token}`));
    } else if (cookies.token !== undefined) {
      dispatch(loadCart(`Bearer ${cookies.token}`));
      dispatch(login(`Bearer ${cookies.token}`));
    }
  }, [allState.OwnerData.email]);

  useEffect(() => {
    const toRequestBody: {[index: string]:string} = {};
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, val] of query.entries()) {
      toRequestBody[key] = val;
    }
    if (Object.keys(toRequestBody)) {
      dispatch(loadBooks(0, toRequestBody));
    } else {
      dispatch(loadBooks(0));
    }
    dispatch(loadAutors());
    dispatch(loadGenres());
    dispatch(loadPrice());
  }, []);

  return (
    <div className="main">
      <header className="main-header" />
      <Books>
        <div className="container">
          <div className="filter-content">
            <Books>
              {((allState.BooksGenres.length && allState.BooksAutors.length) || 0) > 0 ? (
                <>
                  <div className="filter-content"><AutorsSelect /></div>
                  <div className="filter-content"><GenresSelect /></div>
                  <div className="filter-content"><PriceSelect /></div>
                </>
              ) : (
                <>Идет загрузка</>
              )}
            </Books>
          </div>
          <div className="book-content">
            <Typography variant="h5" className="page-title">Список книг</Typography>
            {(allState.Books.count || 0) > 0 ? (
              <>
                <div className="books" id="booksMain">
                  {allState.Books.rows.map((item: any) => (
                    <Paper className="book" key={`${item.id}-book`}>
                      <img
                        src={`${`${REMOTE_URL}fileserver/${item.cover}`}`}
                        alt="без обложки"
                        onClick={() => navigate(`/book/${item.id}`)}
                      />
                      <div>{item.title}</div>
                      <div>{item.autor}</div>
                      <div>{`${item.price} Р`}</div>
                      {
                      // eslint-disable-next-line max-len
                      ((allState.Cart.length > 0) && (allState.Cart.find((el: any) => el.bookUsers.bookId === item.id)))
                        ? (
                          <Button id={`${item.id}-btn`} onClick={removeToCart}>
                            Удалить
                          </Button>
                        )

                        : (
                          <Button id={`${item.id}-btn`} onClick={addToCart}>
                            Добавить
                          </Button>
                        )
                    }
                    </Paper>
                  ))}
                </div>
                <Pagination
                  count={Math.floor(allState.Books.count / 10) + 1}
                  page={currentPage}
                  onChange={(e: any, value: any) => {
                    const toRequestBody: {[index: string]:string} = {};
                    // eslint-disable-next-line no-restricted-syntax
                    for (const [key, val] of query.entries()) {
                      toRequestBody[key] = val;
                    }
                    if (Object.keys(toRequestBody)) {
                      dispatch(loadBooks(value * 10 - 10, toRequestBody));
                    } else {
                      dispatch(loadBooks(value * 10 - 10));
                    }
                    setPage(value);
                  }}
                />
              </>
            ) : (
              <>Идет загрузка каталога</>
            )}
          </div>
        </div>
      </Books>
      <Books>
        <div className="main-footer">
          <Typography className="page-title">
            Все цены на сайте указаны в российских рублях с учетом НДС и без учета стоимости доставки.
            Цены на сайте могут отличаться от цен в розничных магазинах.
            Опция бесплатной доставки от 3000 рублей не распространяется на срочный способ доставки.
          </Typography>
        </div>
      </Books>
    </div>
  );
};

function mapStateToProps(state: RootState) {
  return { allState: state };
}

export default connect(mapStateToProps, null)(Main);
