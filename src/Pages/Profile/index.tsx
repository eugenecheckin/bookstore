import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Button, Typography } from '@mui/material';

import { RootState } from '../../Store/store';
import { uploadAvatar } from '../../Thunk';
import { REMOTE_URL } from '../../config';

import Profile from './style';
import logo from './defaultAvatar.svg';

// eslint-disable-next-line react/function-component-definition
const ProfilePage: React.FC = () => {
  const dispatch = useDispatch();

  const [avatarState, setAvatarState] = useState({});
  const [IsShowAvatar, setIsShow] = useState(true);

  const onChangeInput = (e: any) => {
    if (!e.target.files) return;
    const file = e.target.files[0];
    setAvatarState(file);
    setIsShow(() => false);
    // eslint-disable-next-line no-undef
    const preview: HTMLImageElement = document.getElementById(
      'prevLogo',
    // eslint-disable-next-line no-undef
    ) as HTMLImageElement;
    // eslint-disable-next-line no-undef
    const reader = new FileReader();

    reader.onloadend = () => {
      preview.src = reader.result as string;
    };
    if (file) {
      reader.readAsDataURL(file);
    } else {
      preview.src = '';
    }
  };
  const ownerData = useSelector((state: RootState) => state.OwnerData);

  const buttonClickEvHandler = () => {
    dispatch(uploadAvatar(avatarState, `Bearer ${ownerData.token}`));
    // eslint-disable-next-line no-undef
    const preview: HTMLImageElement = document.getElementById(
      'prevLogo',
    // eslint-disable-next-line no-undef
    ) as HTMLImageElement;

    preview.src = '';
    setIsShow(true);
  };

  const avatar = ownerData.avatar
    ? `${`${REMOTE_URL}fileserver/${ownerData.avatar}`}`
    : logo;

  return (
    <Profile>
      <Typography variant="h5" className="page-title">
        Профиль пользователя
      </Typography>
      <div className="profile-container">
        <img id="prevLogo" src="" alt="" />
        {IsShowAvatar && <img src={avatar} alt="" />}
        <legend>Аватар</legend>
        <input type="file" name="filedata" onChange={onChangeInput} />
        <Button onClick={buttonClickEvHandler}>Загрузить</Button>
        <Typography variant="h6" className="login">
          Имя:
          {ownerData.name}
        </Typography>
        <Typography variant="h6" className="phone">
          Номер телефона:
          {ownerData.phone}
        </Typography>
        <Typography variant="h6" className="email">
          email:
          {ownerData.email}
        </Typography>
      </div>
    </Profile>
  );
};

export default ProfilePage;
