import styled from 'styled-components';

const Profile = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .profile-container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .page-title {
    padding: 5px;
    margin: 0 auto;
  }
`;

export default Profile;
