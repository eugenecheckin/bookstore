import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { Button, Input, Typography } from '@mui/material';

import { useNavigate } from 'react-router-dom';
import { signIn } from '../../Thunk';
import SignIn from './style';

// eslint-disable-next-line react/function-component-definition
const SignInPage: React.FC = () => {
  const dispatch = useDispatch();
  const [loginData, setLoginData] = useState({ email: '', password: '' });

  const navigate = useNavigate();

  const buttonClickEvHandler = () => {
    dispatch(signIn(loginData));
    navigate('/');
  };

  return (
    <SignIn>
      <Typography variant="h5" className="page-title">
        Страница авторизации
      </Typography>
      <div className="signin-container">
        <Input
          type="text"
          className="add-login"
          placeholder="Email"
          onChange={(e) => setLoginData((prev) => ({ ...prev, email: e.target.value }))}
        />
        <Input
          type="text"
          className="add-password"
          placeholder="Password"
          onChange={(e) => setLoginData((prev) => ({ ...prev, password: e.target.value }))}
        />
        <Button onClick={buttonClickEvHandler}>OK</Button>
      </div>
    </SignIn>
  );
};

export default SignInPage;
