import styled from 'styled-components';

const SignIn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .signin-container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .page-title {
    padding: 5px;
    margin: 0 auto;
  }
`;

export default SignIn;
