import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { Button, Input, Typography } from '@mui/material';

import { signUp } from '../../Thunk';
import SignUP from './style';

// eslint-disable-next-line react/function-component-definition
const SignUpPage: React.FC = () => {
  const dispatch = useDispatch();

  const [signUpData, setLoginData] = useState({ email: '', password: '' });

  const buttonClickEvHandler = () => {
    dispatch(signUp(signUpData));
  };

  return (
    <SignUP>
      <Typography variant="h5" className="page-title">
        Страница регистрации
      </Typography>
      <div className="signup-container">
        <Input
          type="text"
          className="add-login"
          placeholder="Имя"
          onChange={(e) => setLoginData((prev) => ({ ...prev, fullName: e.target.value }))}
        />
        <Input
          type="text"
          className="add-phone"
          placeholder="Номер телефона"
          onChange={(e) => setLoginData((prev) => ({ ...prev, phone: e.target.value }))}
        />
        <Input
          type="text"
          className="add-email"
          placeholder="email"
          onChange={(e) => setLoginData((prev) => ({ ...prev, email: e.target.value }))}
        />
        <Input
          type="text"
          className="add-password"
          placeholder="Пароль"
          onChange={(e) => setLoginData((prev) => ({ ...prev, password: e.target.value }))}
        />
        <Button onClick={buttonClickEvHandler}>Зарегистрироваться</Button>
      </div>
    </SignUP>
  );
};

export default SignUpPage;
