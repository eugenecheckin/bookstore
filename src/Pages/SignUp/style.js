import styled from 'styled-components';

const SignUP = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .signup-container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .page-title {
    padding: 5px;
    margin: 0 auto;
  }
`;

export default SignUP;
