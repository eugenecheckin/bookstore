import styled from 'styled-components';

const Wrapper = styled.div`
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  box-sizing: border-box;
  color: #4d4d4d;
  max-width: 1189px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export default Wrapper;
