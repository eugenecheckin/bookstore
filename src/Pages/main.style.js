import styled from 'styled-components';

const Books = styled.div`
  .books {
    display: flex;
    flex-wrap: wrap;
  }
  .book {
    padding: 5px;
    margin: 5px;
    max-width: 195px;
    border: 1px solid transparent;
    &:hover {border: 1px solid gray;}
  } 
  .book {
    display: flex;
    flex-direction: column;
    align-items: center;
    
  }  
  
  .container {
    display: flex;
    flex-direction: row;
    align-items: start;
  }
  .filter-content {
    padding: 8px;
    margin: 8px;
    max-width: 100%;
  }
  .filter-content {
    display: flex;
    flex-direction: column;
    align-items: center;;
  }
  .book-content {
    max-width: 100%;
  } 
  .main-footer {
    padding: 20px;
    margin: 30px;
    max-width: 100%;
  } 
`;

export default Books;
