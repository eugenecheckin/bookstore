import {
  GET_OWNER_DATA,
  GET_ADDED_BOOK_DATA,
  SET_OWNER_AVATAR,
  GET_BOOKS,
  GET_ALL_BOOKS,
  SET_AUTORS,
  SET_GENRES,
  SET_PRICE,
  SET_CART,
  SET_COMMENTS,
  SET_RATING,
} from './types';

const initialState: any = {
  OwnerData: {},
  AddedBookData: {},
  Books: {},
  AllBooks: {},
  BooksAutors: [],
  BooksGenres: [],
  Price: {},
  Cart: {},
  Comments: [],
  Rating: [],
};

// eslint-disable-next-line default-param-last
export default function rootReducer(state = initialState, action: any) {
  switch (action.type) {
    case GET_OWNER_DATA:
      return {
        ...state,
        OwnerData: action.payload,
      };
    case SET_OWNER_AVATAR:
      return {
        ...state,
        OwnerData: {
          ...state.OwnerData,
          avatar: action.payload,
        },
      };
    case GET_ADDED_BOOK_DATA:
      return {
        ...state,
        AddedBookData: action.payload,
      };

    case GET_BOOKS:
      return {
        ...state,
        Books: action.payload,
      };
    case GET_ALL_BOOKS:
      return {
        ...state,
        AllBooks: action.payload,
      };
    case SET_AUTORS:
      return {
        ...state,
        BooksAutors: action.payload,
      };
    case SET_GENRES:
      return {
        ...state,
        BooksGenres: action.payload,
      };
    case SET_PRICE:
      return {
        ...state,
        Price: action.payload,
      };
    case SET_CART:
      return {
        ...state,
        Cart: action.payload,
      };
    case SET_COMMENTS:
      return {
        ...state,
        Comments: action.payload,
      };
    case SET_RATING:
      return {
        ...state,
        Rating: action.payload,
      };
    case 'RESET':
      return initialState;
    default:
      return state;
  }
}

export const getOwnerData = (data: any) => ({
  type: GET_OWNER_DATA,
  payload: data,
});
export const getAddedBookData = (data: any) => ({
  type: GET_ADDED_BOOK_DATA,
  payload: data,
});
export const getBooks = (data: any) => ({
  type: GET_BOOKS,
  payload: data,
});
export const getAllBooks = (data: any) => ({
  type: GET_ALL_BOOKS,
  payload: data,
});
export const setAutors = (data: any) => ({
  type: SET_AUTORS,
  payload: data,
});
export const setGenres = (data: any) => ({
  type: SET_GENRES,
  payload: data,
});
export const setPrice = (data: any) => ({
  type: SET_PRICE,
  payload: data,
});
export const setOwnerAvatarData = (data: any) => ({
  type: SET_OWNER_AVATAR,
  payload: data,
});
export const setCart = (data: any) => ({
  type: SET_CART,
  payload: data,
});
export const setComments = (data: any) => ({
  type: SET_COMMENTS,
  payload: data,
});
export const setRating = (data: any) => ({
  type: SET_RATING,
  payload: data,
});
