import {
  postSignIn,
  postLogin,
  postSignUp,
  getTestConn,
  postUploadAvatar,
  postAddBook,
  postLoadBooks,
  postLoadAllBooks,
  getLoadAutors,
  getLoadGenres,
  getLoadPrice,
  postLoadCart,
  postAddCart,
  postRemoveCart,
  postAddCoomment,
  postLoadCoomment,
  postAddRating,
  postLoadRating,
} from '../Api/axios';

import {
  getOwnerData,
  getAddedBookData,
  setOwnerAvatarData,
  getBooks,
  getAllBooks,
  setAutors,
  setGenres,
  setPrice,
  setCart,
  setComments,
  setRating,
} from '../Store/Reducers/rootReduser';

export const signIn = (signinData: any) => async (dispatch: any) => {
  const responce = await postSignIn(signinData);
  dispatch(getOwnerData(responce.data));
};
export const login = (token: any) => async (dispatch: any) => {
  const responce = await postLogin(token);
  dispatch(getOwnerData(responce.data));
};
export const signUp = (signupData: any) => async (dispatch: any) => {
  const responce = await postSignUp(signupData);
  dispatch(getOwnerData(responce.data));
};

export const testConn = () => async (dispatch: any) => {
  const responce = await getTestConn();
  dispatch(getOwnerData(responce.data));
};

export const uploadAvatar = (file: any, token: string) => async (dispatch: any) => {
  const responce = await postUploadAvatar(file, token);
  dispatch(setOwnerAvatarData(responce.data.path));
};

export const addBook = (
  bookUpData: any,
  bookCover: any,
  token: string,
) => async (dispatch: any) => {
  const responce = await postAddBook(bookUpData, bookCover, token);
  dispatch(getAddedBookData(responce.data));
};

export const loadBooks = (offset: number, term = {}) => async (dispatch: any) => {
  const responce = await postLoadBooks(offset, term);
  dispatch(getBooks(responce.data));
};

export const loadCart = (token: string) => async (dispatch: any) => {
  const responce = await postLoadCart(token);
  dispatch(setCart(responce.data));
};

export const addCart = (token: string, id: number) => async (dispatch: any) => {
  const responce = await postAddCart(token, id);
  dispatch(setCart(responce.data));
};
export const removeCart = (token: string, id: number) => async (dispatch: any) => {
  const responce = await postRemoveCart(token, id);
  dispatch(setCart(responce.data));
};

export const loadAllBooks = () => async (dispatch: any) => {
  const responce = await postLoadAllBooks();
  dispatch(getAllBooks(responce.data));
};
export const loadAutors = () => async (dispatch: any) => {
  const responce = await getLoadAutors();
  dispatch(setAutors(responce.data));
};
export const loadGenres = () => async (dispatch: any) => {
  const responce = await getLoadGenres();
  dispatch(setGenres(responce.data));
};
export const loadPrice = () => async (dispatch: any) => {
  const responce = await getLoadPrice();
  dispatch(setPrice(responce.data));
};

export const addComment = (token: string, term = {}) => async (dispatch: any) => {
  const responce = await postAddCoomment(token, term);
  const sortedComments = responce.data.map((item:any) => {
    const modifiedItem = item;
    const finded = responce.data.find((i:any) => i.to === item.id);
    if (finded !== undefined) {
      modifiedItem.from = finded.id;
    } else {
      modifiedItem.from = 0;
    }
    return modifiedItem;
  });
  dispatch(setComments(sortedComments));
};
export const loadComment = (term = {}) => async (dispatch: any) => {
  const responce = await postLoadCoomment(term);
  const sortedComments = responce.data.map((item:any) => {
    const modifiedItem = item;
    const finded = responce.data.find((i:any) => i.to === item.id);
    if (finded !== undefined) {
      modifiedItem.from = finded.id;
    } else {
      modifiedItem.from = 0;
    }
    return modifiedItem;
  });
  dispatch(setComments(sortedComments));
};
export const addRating = (token: string, term = {}) => async (dispatch: any) => {
  const responce = await postAddRating(token, term);
  dispatch(setRating(responce.data));
};
export const loadRating = (term = {}) => async (dispatch: any) => {
  const responce = await postLoadRating(term);
  dispatch(setRating(responce.data));
};
