import axios from 'axios';

import { REMOTE_PORT } from '../config';

export const postSignIn = async (signinData: any) => axios.post(
  `http://localhost:${REMOTE_PORT}/auth/signin`,
  signinData,
);

export const postLogin = async (token: any) => axios.post(
  `http://localhost:${REMOTE_PORT}/auth/login`,
  {},
  {
    headers: {
      authorization: `${token}`,
    },
  },
);

export const postSignUp = async (signupData: any) => axios.post(
  `http://localhost:${REMOTE_PORT}/auth/signup`,
  signupData,
);
// only test
export const getTestConn = async () => axios.get(`http://localhost:${REMOTE_PORT}/auth/test`);

export const postUploadAvatar = async (file: any, token: string) => {
  // eslint-disable-next-line no-undef
  const avatarFormFile = new FormData();
  avatarFormFile.append('file', file);
  return axios.post(
    `http://localhost:${REMOTE_PORT}/auth/upload`,
    avatarFormFile,
    {
      headers: {
        authorization: `${token}`,
      },
    },
  );
};

export const postAddBook = async (
  bookUpData: any,
  bookCover: any,
  token: string,
) => {
  // eslint-disable-next-line no-undef
  const bookFormFile = new FormData();
  bookFormFile.append('file', bookCover);
  bookFormFile.append('title', bookUpData.title);
  bookFormFile.append('autor', bookUpData.autor);
  bookFormFile.append('year', bookUpData.year);
  bookFormFile.append('genre', bookUpData.genre);
  bookFormFile.append('price', bookUpData.price);
  return axios.post(
    `http://localhost:${REMOTE_PORT}/book/create`,
    bookFormFile,
    {
      headers: {
        authorization: `${token}`,
      },
    },
  );
};

export const postLoadBooks = async (offset: number, term = {}) => axios.post(`http://localhost:${REMOTE_PORT}/book/load`, term, {
  headers: {
    offset: `${offset}`,
  },
});

export const postLoadCart = async (token: string) => axios.post(`http://localhost:${REMOTE_PORT}/book/loadcart`, { test: 'тест' }, {
  headers: {
    authorization: `${token}`,
  },
});

export const postAddCart = async (token: string, id: number) => axios.post(`http://localhost:${REMOTE_PORT}/book/addtocart`, { test: 'тест' }, {
  headers: {
    authorization: `${token}`,
    bookid: id,
  },
});
export const postRemoveCart = async (token: string, id: number) => axios.post(`http://localhost:${REMOTE_PORT}/book/removetocart`, { test: 'тест' }, {
  headers: {
    authorization: `${token}`,
    bookid: id,
  },
});
// only test
export const postLoadAllBooks = async () => axios.get(`http://localhost:${REMOTE_PORT}/book/loadall`);

export const getLoadAutors = async () => axios.get(`http://localhost:${REMOTE_PORT}/book/loadautors`);

export const getLoadGenres = async () => axios.get(`http://localhost:${REMOTE_PORT}/book/loadganres`);

export const getLoadPrice = async () => axios.get(`http://localhost:${REMOTE_PORT}/book/price`);

export const postAddCoomment = async (
  token: string,
  term = {},
) => axios.post(
  `http://localhost:${REMOTE_PORT}/book/addcomment`,
  term,
  {
    headers: {
      authorization: `${token}`,
    },
  },
);
export const postLoadCoomment = async (
  term = {},
) => axios.post(
  `http://localhost:${REMOTE_PORT}/book/loadcomment`,
  term,
);
export const postAddRating = async (
  token: string,
  term = {},
) => axios.post(
  `http://localhost:${REMOTE_PORT}/book/addrating`,
  term,
  {
    headers: {
      authorization: `${token}`,
    },
  },
);
export const postLoadRating = async (
  term = {},
) => axios.post(
  `http://localhost:${REMOTE_PORT}/book/loadrating`,
  term,
);
