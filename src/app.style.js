import styled from 'styled-components';

const AppHeader = styled.div`
  .app-header {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    margin: 0 auto;
  }
  .link-header {
    list-style: none;
    margin: 0px;
    padding: 0px;
  }
`;

export default AppHeader;
